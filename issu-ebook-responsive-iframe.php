<?php
/*
Plugin Name: Issue eBook Responsive iFrame
*/
class IssueIframe {
	public function __construct() {
		add_action('wp_head',array($this,'inject_script'));
	}
	public function inject_script() { ?>
		<script type="text/javascript">
			jQuery(document).ready(function($){
				function adjustIframes() {
					$('object').each(function(){
						var
						$this       = $(this),
						actual_w	= $this.width(),
						proportion  = 1100 / 850;

						$this.css( 'height', Math.round( actual_w * proportion * .6) + 'px' );
					});
				}
				$(window).on('load resize',adjustIframes);
			});
		</script>
	<?php }
}
new IssueIframe();
?>